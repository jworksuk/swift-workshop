//
//  Home.swift
//  MyApp
//
//  Created by Jesse James on 09/11/2014.
//  Copyright (c) 2014 Jesse James. All rights reserved.
//

import UIKit

class Home: UIViewController {

    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var headerVisualEffectView: UIVisualEffectView!
    @IBAction func imageButtonDidPress(sender: AnyObject) {
        
        springWithCompletion(0.7, {
            self.imageButton.frame = CGRectMake(0, 0, 320, 240)
            self.dialogView.frame = CGRectMake(0, 0, 320, 568)
            self.likeButton.alpha = 0
            self.shareButton.alpha = 0
            self.headerVisualEffectView.alpha = 0
            self.dialogView.layer.cornerRadius = 0
        }, { finished in
            self.performSegueWithIdentifier("homeToDetail", sender: nil)
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // begin
        let scale = CGAffineTransformMakeScale(0.5, 0.5)
        let translate = CGAffineTransformMakeTranslation(0, -200)
        dialogView.transform = CGAffineTransformConcat(scale, translate)

        
        spring(0.7){
            // end
            self.dialogView.transform = CGAffineTransformIdentity
        }
    }

}
